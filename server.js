const http = require('http')
const fs = require('fs')
const browserify = require('browserify')
const webSocket = require('websocket')

const path = 'hello-world'
const webSocketConnections = []

const httpServer = http.createServer((req, res) => sendResource(req, res))

const webSocketServer = new webSocket.server({httpServer: httpServer})

webSocketServer.on('request', req =>
    webSocketConnections.push(
        req.accept('refresh', req.origin)))

httpServer.listen(3000, 'localhost')

fs.watch(path, (event, filename) => {
    if (filename.endsWith('.js') && !filename.endsWith('.browserified.js'))
        browserifyFile(path, /^(.+).js$/.exec(filename)[1])
    else
        sendRefresh()
})

function browserifyFile(path, filename) {
    browserify()
        .add(`${path}/${filename}.js`)
        .bundle(sendRefresh())
        .pipe(fs.createWriteStream(`${path}/${filename}.browserified.js`))
}

function sendResource(req, res) {

    const url = req.url

    if (url === '/')
        fs.readFile(`${path}/index.html`, 'utf8', (_, html) => res.end(injectWebSocketRefresh(html)))
    else {
        if (url.endsWith('.css'))
            res.setHeader('Content-Type', 'text/css')
        else if (url.split('?')[0].endsWith('.js'))
            res.setHeader('Content-Type', 'application/javascript')

        fs.readFile(`${path}${req.url}`, 'utf8', (_, html) => res.end(html))
    }

}

function sendRefresh() {
    webSocketConnections.forEach(connection => connection.sendUTF("refresh"))
}

function injectWebSocketRefresh(html) {
    const injectionIndex = html.lastIndexOf('</body>')
    const htmlToInject = `<script>
            var connection = new WebSocket('ws://localhost:3000/', 'refresh');
            connection.onmessage = function() {
                location.reload();
            }</script>`
    return html.substr(0, injectionIndex) + htmlToInject + html.substr(injectionIndex)
}
